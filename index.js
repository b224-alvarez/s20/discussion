// REPETITION CONTROL STRUCTURES
/* 
    While loop

        Syntax: 

            -in while loop, if the coniditon is true, it executes the code/statement until the condition is no longer true.

            While(expression/condition) {
                statement
            };


*/

let count = 5;

while (count !== 0) {
  console.log("While: " + count);
  count--; //count -= 1
}

/* 
    Mini Activity

*/

let x = 1;

while (x < 6) {
  console.log(x);
  5;
  x++;
}

/* 
    Do While Loop
        - A do-while loop works a lot like the while loop. But unlike while loop, do-while loop guarantees the code will be executed a t least once.

        Syntax:
            do {
                statement;
            } while (expression/condition) 

*/

// let number = Number(prompt("Give me a number: "));

// do {
//   console.log("Do while: " + number);
//   number += 1;
// } while (number < 10);

/* 
    FOR LOOP
        -more flexible than while loop and do-while loop

        Syntax:
            for (initialization; expression/condition; finalExpression) {
                statement;
            }

*/

for (let count = 0; count <= 20; count++) {
  console.log("For Loop: " + count);
}

//  The ".length" property

// Characters in strings may be counted using the .length property.
// Strings are special compared to other data types in a way that it has access to functions and other pieces of information, another primitive data might not have.
let myString = "Iron Man";
console.log(myString.length);

// Access characters of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[7]);

console.log(myString[myString.length - 1]);

for (let i = 0; i < myString.length; i++) {
  console.log(myString[i]);
}

let myName = "FERNANDO";

for (let i = 0; i < myName.length; i++) {
  if (
    myName[i].toLowerCase() == "a" ||
    myName[i].toLowerCase() == "e" ||
    myName[i].toLowerCase() == "i" ||
    myName[i].toLowerCase() == "o" ||
    myName[i].toLowerCase() == "u"
  ) {
    console.log("Vowel");
  } else {
    console.log(myName[i]);
  }
}

let aString = "extravagant";
let aStringCons = "";

for (let i = 0; i < aString.length; i++) {
  if (
    aString[i].toLowerCase() == "a" ||
    aString[i].toLowerCase() == "e" ||
    aString[i].toLowerCase() == "i" ||
    aString[i].toLowerCase() == "o" ||
    aString[i].toLowerCase() == "u"
  ) {
    continue;
  } else {
    aStringCons += aString[i];
  }
}
console.log(aStringCons);

// Continue and Break Statements

/* 
    "continue" statmement allows the code to go to the next iteration of the loop without finishing the execution of all the statement in a code block.

    "break" statement is used to terminate the current loop once a match has been found

*/

for (let count = 0; count <= 20; count++) {
  console.log("Hello World: " + count);
  if (count % 2 === 0) {
    console.log("Even Number");
    continue;
  }

  if (count > 10) {
    break;
  }
}

let name1 = "Alejandro";
for (let i = 0; i < name1.length; i++) {
  console.log(name1[i]);

  if (name1[i].toLowerCase() === "a") {
    console.log("Continue to the next iteration");
    continue;
  }

  if (name1[i].toLowerCase() === "d") {
    break;
  }
}
